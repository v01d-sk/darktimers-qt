CONFIG += qt link_pkgconfig c++11
PKGCONFIG += libssh2
QT += core gui widgets

SOURCES += $$PWD$$/src/*.cpp
SOURCES += $$PWD$$/libsftp/src/*.c

HEADERS += $$PWD$$/src/*.h
HEADERS += $$PWD$$/libsftp/include/*.h

FORMS += $$PWD$$/src/*.ui

INCLUDEPATH += $$PWD$$/libsftp/include
