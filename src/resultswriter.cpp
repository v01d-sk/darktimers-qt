#include <cerrno>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <algorithm>
#include <iomanip>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include <QDir>
#include <QFile>

#include "resultswriter.h"

constexpr uint32_t TIME_DECIMALS = 2;
constexpr uint32_t TIME_WIDTH = 5;

/** Unpack a float timestamp in seconds to seconds/minutes.
 *
 * XXX TODO ASAP XXX remove this and use only unpack_timestamp
 * (multiply hours by 60 to get minutes), this was kept only because
 * of a need to quickly fix bugs without creating new ones.
 */
static void unpack_timestamp_no_hours( double time, int& m, double& s )
{
    int time_int = static_cast<int>(time);
    const double time_remainder = time - time_int;
    m = time_int / 60;
    time_int -= m * 60;
    s = time_int + time_remainder;
}


/** Unpack a float timestamp in seconds to hours/seconds/minutes.
 */
void unpack_timestamp( double time, int& h, int& m, double& s )
{
    int time_int = static_cast<int>(time);
    const double time_remainder = time - time_int;
    h = time_int / 3600;
    time_int -= h * 3600;
    m = time_int / 60;
    time_int -= m * 60;
    s = time_int + time_remainder;
}


// TODO with new enough GCC/libstdc++ this is in the standard library in <string>.
// Remove this at some point in future.
std::string to_string( const uint32_t i )
{
    /* This is approximately 42000x slower than it needs to be.
     * Good enough.*/
    std::stringstream ss;
    ss << i;
    return ss.str();
}

std::string markdown_fix_newlines( const std::string& src )
{
    std::stringstream ss(src);
    std::string result;
    std::string part;
    while(std::getline(ss, part, '\n'))
    {
        result += part + "\\\n";
    }
    return result;
}

bool ResultsWriter::write( std::string out_dir_path, std::string out_format )
{
    if (this->data.stage_names.empty())
    {
        fprintf(stderr, "No stage names! \n");
        return false;
    }
    if (this->data.categories.empty())
    {
        fprintf(stderr, "No categories! \n");
        return false;
    }

    auto gen_file = [&]( const std::string& category, const std::string& base,
                         const uint32_t stage_idx, const bool cumulative )
    {
        const bool use_crosslinks = (out_format == "html");

        auto dir = out_dir_path + "/" + out_format;
        if (!QDir(dir.c_str()).exists())
        {
            QDir().mkdir(dir.c_str());
        }

        const auto md_path       = dir + "/" + base + ".md";
        const auto out_path      = dir + "/" + base + "." + out_format;
        const auto out_logo_path = dir + "/" + this->data.logo_path;
        const auto markdown      = generate_markdown(category, stage_idx,
                                                     cumulative, use_crosslinks);

        if( !write_markdown(md_path, markdown ) ||
            !run_pandoc( md_path, out_path, out_logo_path, out_format, "") )
        {
            remove( md_path.c_str() );
            return false;
        }
        remove( md_path.c_str() );
        return true;
    };

    const uint32_t last_stage_idx = this->data.stage_names.size() - 1;
    // TODO move "absolute", "stage" and "category" (used for filenames) info constexpr
    gen_file( "", "absolute", last_stage_idx, true );
    // per category
    for (uint32_t i = 0; i < this->data.categories.size(); ++i)
    {
        auto& category = this->data.categories[i];
        gen_file( category, "category_" + to_string(i), last_stage_idx, true );
    }
    // per stage
    for (uint32_t i = 0; i <= last_stage_idx; ++i)
    {
        gen_file( "", "stage_" + to_string(i), i, false );
    }
    return true;
}

std::string ResultsWriter::generate_markdown
    ( std::string category,
      const uint32_t stage_idx,
      const bool cumulate_times,
      const bool crosslinks )
{
    // TODO have an is_valid function that will refuse to generate anything
    //     if any racer has not enough stage_times
    auto calc_time = [&]( const Racer& r )
    {
        const std::vector<double>& times(r.stage_times);

        if (!cumulate_times)
        {
            return times[stage_idx];
        }
        // TODO test that this sum is correct
        // and that the items are sorted correctly.
        return std::accumulate(times.begin(), (times.begin() + stage_idx + 1), 0.0);
    };

    // sort the racers (they may not be sorted by the stage we want, etc.)
    std::sort(this->data.racers.begin(), this->data.racers.end(),
        [&](Racer& a, Racer& b){ return calc_time(a) < calc_time(b); });


    /* logo
     * name
     * place
     *
     * results table
     *
     * description
     * date*/

    std::stringstream md;
    ResultsData r;

    // logo
    md << "![](" << this->data.logo_path << " \"\")\n";
    md << "\n\n"; // not sure about this one
    // name
    md << "#" << this->data.race_name << "\n";
    md << "\n\n";
    md << "#" << this->data.race_location << "\n";
    md << "\n\n";

    if (crosslinks)
    {
        // TODO unhardcode "Absolut" here this word should be set by the user
        md << "[" << "Absolut" << "]";
        md << "(" << "absolute" << ".html" << ") ";
        // TODO move "category_" + to_string(i) and such into functions,
        //      called from here and ResultsWriter::write()
        for (uint32_t i = 0; i < this->data.categories.size(); ++i)
        {
            md << "[" << this->data.categories[i] << "]";
            md << "(" << "category_" + to_string(i) << ".html" << ") ";
        }
        md << "\n\n";
        for (uint32_t i = 0; i < this->data.stage_names.size(); ++i)
        {
            md << "[" << this->data.stage_names[i] << "]";
            md << "(" << "stage_" + to_string(i) << ".html" << ") ";
        }
        md << "\n\n";
    }

    // results table
    // TODO-ASAP un-hardcode this - internationalization struct
    md << "|Poradie | Číslo | Meno | Ročník |  Tím  | Kategória | Čas | Strata |\n";
    md << "|:------:|------:|:-----|:------:|:------|:----------|----:|-------:|\n";

    double time_first;
    // TODO refactor this
    uint32_t i = 0;
    for (const auto& r: this->data.racers)
    {
        if ( r.disqualified || (!category.empty() && r.category != category ))
        {
            continue;
        }

        const double time = calc_time( r );

        if ( 0 == i )
        {
            time_first = time;
        }
        const double loss = time - time_first;

        int time_m; double time_s;
        unpack_timestamp_no_hours( time, time_m, time_s );
        int loss_m; double loss_s;
        unpack_timestamp_no_hours( loss, loss_m, loss_s );

        md << "|" << i + 1
           << "|" << r.number
           << "|" << r.name
           << "|" << r.year
           << "|" << r.team
           << "|" << r.category
           // 0:26:29.6
           << std::setiosflags(std::ios::fixed) << std::setprecision(TIME_DECIMALS)
           << "|" << time_m  << ":" << std::setfill('0') << std::setw(TIME_WIDTH) << time_s
           << "|" << loss_m  << ":" << std::setfill('0') << std::setw(TIME_WIDTH) << loss_s
           << "|\n";
        ++i;
    }

    for (const auto& r: this->data.racers)
    {
        if ( !r.disqualified || (!category.empty() && r.category != category ))
        {
            continue;
        }

        md << "|---|" << r.number
           << "|" << r.name
           << "|" << r.year
           << "|" << r.team
           << "|" << r.category
           << "|" << r.disqualified_reason
           << "|---|\n";
    }

    // race description & date
    md << "\n\n";
    md << markdown_fix_newlines(this->data.race_description);
    md << "\n\n";
    md << this->data.race_date;

    return md.str();
}


bool ResultsWriter::write_markdown( const std::string& path, const std::string& markdown )
{
    FILE* file = fopen(path.c_str(), "w");
    if (0 == file)
    {
        fprintf(stderr, "Failed to open '%s': %s", path.c_str(), strerror(errno));
        return false;
    }

    if(markdown.size() != fwrite(markdown.c_str(), 1, markdown.size(), file))
    {
        perror("Failed to write to file");
        return false;
    }

    if (ferror(file))
    {
        fprintf(stderr, "Error writing '%s': %s", path.c_str(), strerror(errno));
    }

    fclose(file);
    return true;
}

// TODO: how will we handle the case where some racers don't have times at some stages?
//       (maybe caller handles this already)

bool ResultsWriter::run_pandoc( std::string in_path,
        std::string out_path,
        std::string out_logo_path,
        std::string out_format,
        std::string extra_args )
{
    // pandoc $extra_arg -t $out_fornmat -o $out_path $in_path
    // needed to prevent pandoc from captioning the logo
    extra_args += " -fmarkdown-implicit_figures";
    if (out_format == "pdf" )
    {
        out_format = "latex";
    }
    if (out_format == "html")
    {
        //mv this->data.logo_path this->data.out_dir
        extra_args += " -s";
        // copy logo to output directory as pandoc does not do that for HTML
        if (QFile::exists(out_logo_path.c_str()))
        {
            QFile::remove(out_logo_path.c_str());
        }
        printf("copying '%s' to '%s'\n", this->data.logo_path.c_str(), out_logo_path.c_str() );
        QFile::copy( this->data.logo_path.c_str(), out_logo_path.c_str() );
    }
    // TODO what about filenames with double quotes?
    // wrapping paths in quotes as they may contain spaces.
    std::string command = pandoc_path + " " + extra_args + " -t " + out_format
        + " -o \"" + out_path + "\" \"" + in_path + "\"";

    fprintf(stderr, "running %s\n", command.c_str());

    // TODO we need to do this async (maybe just spawning a thread here to run
    //      pandoc would be enough, with an ability to check later if an error occured)
    int return_code = std::system( command.c_str() );
    if (0 == return_code)
    {
        return true;
    }

    fprintf(stderr, "Error in pandoc process "
            "(in_path %s, out_path %s, out_format %s, extra_args %s)\n",
            in_path.c_str(), out_path.c_str(), out_format.c_str(), extra_args.c_str());
    return false;
}
