#include <QLabel>
#include <QProgressBar>
#include <QAbstractButton>
#include <QDialogButtonBox>

#include "progressbar.h"
#include "ui_progressbar.h"


ProgressBarWin::ProgressBarWin(QString Title, QString Text, bool cancel, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressBarWindow)
{
    ui->setupUi(this);
    setWindowTitle(Title);
    QLabel *ui_progressbar_text = findChild<QLabel*>("progressbar_text");
    ui_progressbar_text->setText(Text);

    this->ui_progressbar = findChild<QProgressBar*>("progressbar");
    if (!cancel)
    {
        QDialogButtonBox *ui_button_box = findChild<QDialogButtonBox*>("button_box");
        ui_button_box->setVisible(false);
    }
}

ProgressBarWin::~ProgressBarWin()
{
    delete ui;
}

void ProgressBarWin::on_button_box_clicked(QAbstractButton *button)
{
    this->close();
}

void ProgressBarWin::set(int value)
{
    this->ui_progressbar->setValue(value);
}

