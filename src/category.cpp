#include <QLineEdit>
#include <QAbstractButton>

#include "category.h"
#include "ui_category.h"


CategoryWin::CategoryWin(QString title, QString name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CategoryWindow)
{
    ui->setupUi(this);
    setWindowTitle(title);
    this->ui_category_name = findChild<QLineEdit*>("category_name");
    this->ui_category_name->setText(name);
}

CategoryWin::~CategoryWin()
{
    delete ui;
}

QString CategoryWin::getCategoryName()
{
    return this->ui_category_name->text();
}
void CategoryWin::on_button_box_clicked(QAbstractButton *button)
{
    if (button->text() != "OK")
        this->ui_category_name->setText("");
}


