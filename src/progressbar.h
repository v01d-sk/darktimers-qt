#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QDialog>
#include <QString>

namespace Ui
{
    class ProgressBarWindow;
};

class QProgressBar;
class QAbstractButton;

class ProgressBarWin : public QDialog
{
    Q_OBJECT

public:
    explicit ProgressBarWin(QString Title, QString Text, bool cancelButton, QWidget *parent = 0);
    ~ProgressBarWin();
    void set(int value);

private slots:
    void on_button_box_clicked(QAbstractButton *button);

private:
    QProgressBar *ui_progressbar;

    Ui::ProgressBarWindow *ui;
};
#endif

