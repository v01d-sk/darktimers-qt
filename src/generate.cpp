#include <QString>
#include <string>
#include <vector>
#include "internal_data.h"
#include "resultswriter.h"
#include "generate.h"

GenerateResults::GenerateResults(EventData *e, SettingsData *s):
    eventData(e),
    settings(s)
    
{
}


Racer GenerateResults::CreateRacer(EventDataContainer *data)
{
    Racer racer;
    racer.name = data->racer.Name.toStdString();
    racer.team = data->racer.Team.toStdString();
    racer.category = settings->categories.at(data->racer.Category).toStdString();
    racer.disqualified_reason = "###";
    racer.number = data->racer.StartNum;
    racer.year = data->racer.BirthYear;
    racer.disqualified = data->dqf;

    // In seconds. **Must** be as long as ResultsData.stage_names. Otherwise segfault (ATM).
    racer.stage_times.insert(racer.stage_times.end(), data->stagesTimes.begin(), data->stagesTimes.end());
    return racer;
}

void GenerateResults::Execute()
{
    ResultsData data;
    data.race_name = settings->race_name.toStdString();
    data.race_description = settings->race_organizer.toStdString();
    data.race_date = std::to_string(settings->race_year) + "/" + std::to_string(settings->race_month) + "/" + std::to_string(settings->race_day);
    data.race_location = settings->race_location.toStdString();
    data.logo_path = settings->race_logo.toStdString();
    std::vector<QString>::const_iterator iter = settings->race_stage_names.begin();
    for ( ; iter != settings->race_stage_names.end(); ++iter)
    {
        data.stage_names.push_back(iter->toStdString());
    }
    iter = settings->categories.begin();
    for ( ; iter != settings->categories.end(); ++iter)
    {
        data.categories.push_back(iter->toStdString());
    }
    std::vector<EventDataContainer *>results =  this->eventData->getResults();
    std::vector<EventDataContainer *>::iterator result_iter = results.begin();
    for (; result_iter < results.end(); ++result_iter)
    {
        data.racers.push_back(CreateRacer(*result_iter));
    }
    ResultsWriter writer(data,settings->interface_pandoc_path.toStdString());
    writer.write(settings->interface_result_path.toStdString(),"html");
    writer.write(settings->interface_result_path.toStdString(),"odt");
}



