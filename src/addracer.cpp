#include <QStringList>
#include <QComboBox>
#include <QListWidget>
#include <QAbstractButton>

#include <stdio.h>
#include "addracer.h"
#include "ui_addracer.h"
#include "internal_data.h"
#include "lineedit.h"

AddRacerWin::AddRacerWin(SettingsData *settings_data, RacerData *data, QWidget *parent):
    QDialog(parent),
    ui(new Ui::AddRacerWindow),
    settings(settings_data),
    racer(NULL)
{
    ui->setupUi(this);
    this->ui_rfid_list = findChild<QListWidget*>("rfid_list");
    this->ui_racer_name = findChild<QLineEdit*>("racer_name");
    this->ui_start_number = findChild<QLineEdit*>("start_number");
    this->ui_team = findChild<QLineEdit*>("team");
    this->ui_birth_year = findChild<QLineEdit*>("birth_year");
    this->ui_category = findChild<QComboBox*>("category");
    for (int i=0; i < settings->categories.size(); i++)
    {
        this->ui_category->addItem(settings->categories.at(i));
    }
    this->ui_country = findChild<QLineEdit*>("country");    
    if (data != NULL)
    {
        std::vector<QString> rfids = data->Rfids();
        std::vector<QString>::iterator iter = rfids.begin();
        for (; iter != rfids.end(); ++iter)
        {
            this->ui_rfid_list->addItem(*iter);
        }
        this->ui_start_number->setText(QString::number(data->StartNum));
        this->ui_racer_name->setText(data->Name);
        this->ui_team->setText(data->Team);
        this->ui_birth_year->setText(QString::number(data->BirthYear));
        this->ui_category->setCurrentIndex(data->Category);
        this->ui_country->setText(data->Country);
        this->setWindowTitle("Edit Racer");
    }
}

AddRacerWin::~AddRacerWin()
{
    if (this->racer != NULL)
        delete this->racer;
    delete this->ui;
}

RacerData *AddRacerWin::getRacerData()
{
    return (this->racer);
}

void AddRacerWin::on_add_rfid_clicked()
{
    LineEditWin win("Add RFID","Swipe coin");
    win.exec();
    QString rfid = win.value();
    if (!rfid.isEmpty())
        this->ui_rfid_list->addItem(win.value());
}

void AddRacerWin::on_del_rfid_clicked()
{
    this->ui_rfid_list->takeItem(this->ui_rfid_list->currentRow());
}

void AddRacerWin::on_button_box_clicked(QAbstractButton *button)
{
    if (button->text() == "OK")
    {
        this->racer = new RacerData();
        racer->Name = this->ui_racer_name->text();
        racer->Team = this->ui_team->text();
        racer->Country = this->ui_country->text();
        racer->StartNum = this->ui_start_number->text().toUInt();
        std::vector<QString> rfids;
        for (int i=0; i < this->ui_rfid_list->count(); i++)
        {
            rfids.push_back(this->ui_rfid_list->item(i)->text());
        }
        racer->SetRfid (rfids);
        racer->BirthYear = this->ui_birth_year->text().toUInt();
        racer->Category = this->ui_category->currentIndex();
    }
}
