#ifndef INTERNAL_DATA_H
#define INTERNAL_DATA_H

class SettingsData
{
    public:
        SettingsData();
        ~SettingsData();
        int CategoryId(QString category);

        int Save(QString filename);
        int Load(QString filename);

        QString race_name;
        QString race_organizer;
        QString race_location;
        int race_day;
        int race_month;
        int race_year;
        QString race_logo;
        std::vector<QString> race_stage_names;
        std::vector<QString> categories;

        QString connection_server;
        QString connection_login;
        QString connection_password;

        QString interface_result_path;
        QString interface_pandoc_path;
};

class RacerData
{
    public:
        RacerData(QString line);
        RacerData();
        const QString toString() const;
        const QString strRfid() const;
        std::vector<QString> Rfids() {return this->Rfid;};
        void SetRfid(const QString &rfid);
        void SetRfid(const std::vector<QString> &rfid);
        unsigned int StartNum;
        QString Name;
        unsigned int Category;
        unsigned int BirthYear;
        QString Team;
        QString Country;
        bool operator == (const QString &Rfid) const;
        bool operator != (const QString &Rfid) const;

        bool operator == (unsigned int) const;
        bool operator != (unsigned int) const;

    private:
        std::vector<QString> Rfid;
};

struct EventDataContainer
{
    EventDataContainer(const RacerData &r, const std::vector<double> &t, bool d);
    double getOveral();
    const RacerData &racer;
    std::vector<double> stagesTimes;
    bool dqf;
};

class EventData
{
    public:
        EventData();
        ~EventData();
        unsigned int size();
        void AddRacerResults(const RacerData &data, const std::vector<double> &times, bool dqf);
        std::vector<EventDataContainer *> getResults();
    private:
        std::vector<EventDataContainer *> results;
        SettingsData *settings;

};

#endif
