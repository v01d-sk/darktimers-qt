#ifndef MAINWIN_H
#define MAINWIN_H

#include <QMainWindow>
#include "internal_data.h"

namespace Ui {
    class MainWindow;
}

class SettingsData;
class FillDataWin;

class MainWin : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWin(QWidget *parent = 0);
    ~MainWin();

private slots:
    void on_fill_data_clicked();
    void on_read_data_clicked();
    void on_process_data_clicked();
    void on_generate_results_clicked();

    void on_settings_clicked();
    void on_exit_clicked();
private:
    Ui::MainWindow *ui;
    SettingsData *settings_data;
    std::vector<QString> filenames;
    std::vector<RacerData> racers;
    FillDataWin *filldatawin;
    EventData *eventData;
};

#endif // MAINWIN_H
