#ifndef RESULTSWRITER_H_YBISRCVG
#define RESULTSWRITER_H_YBISRCVG

#include <cstdint>
#include <string>
#include <vector>

struct Racer
{
    std::string name;
    std::string team;
    std::string category;
    std::string disqualified_reason;
    uint32_t number;
    uint32_t year;
    bool disqualified;

    // In seconds. **Must** be as long as ResultsData.stage_names. Otherwise segfault (ATM).
    std::vector<double> stage_times;
};

/**
 *
 * Example:
 *
 * ```
 * ResultsData data;
 * data.race_name = "Blah blah blah";
 * data.race_description =
 *     "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor "
 *     "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
 *     "exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure "
 *     "dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.";
 * data.race_date = "32.13.201A";
 * data.logo_path = "logo.png";
 * for (uint32_t i = 0; i < 5; ++i)
 * {
 *     std::stringstream stage_name;
 *     stage_name << "Stage " << i;
 *     data.stage_names.push_back(stage_name.str());
 * }
 * data.categories.push_back("Muži");
 * data.categories.push_back("Ženy");
 * data.categories.push_back("Hermafroditi");
 *
 * for (uint32_t i = 0; i < 20; ++i)
 * {
 *     Racer racer;
 *
 *     std::stringstream racer_name;
 *     racer_name << "Racer " << i;
 *     racer.name = racer_name.str();
 *
 *     racer.team = "Dummy team";
 *     racer.category = data.categories[i % data.categories.size()];
 *     racer.number = i;
 *     racer.year = 1942;
 *     racer.disqualified = i <= 17 ? false : true;
 *     for (uint32_t j = 0; j < data.stage_names.size(); ++j)
 *     {
 *         racer.stage_times.push_back( 100 + i * 4.23 + j * 5.2) ;
 *     }
 *
 *     data.racers.push_back(racer);
 * }
 * ```
 */
struct ResultsData
{
    std::string race_name;
    std::string race_description;
    std::string race_date;
    std::string race_location;
    std::string logo_path;
    std::vector<std::string> stage_names;
    std::vector<std::string> categories;
    std::vector<Racer> racers;
};

/**
 * Example:
 *
 * ```
 * ResultsWriter writer( generate_dummy_results(), "/usr/bin/pandoc" );
 * writer.write( "./test-out", "html");
 * ```
 */

void unpack_timestamp( double time, int& h, int& m, double& s );

class ResultsWriter final
{
public:
    // Copy everything, performance is not a concern here.
    explicit ResultsWriter( const ResultsData& data,
                            const std::string& pandoc_path ) noexcept
        : data( data )
        , pandoc_path( pandoc_path )
    {
    }

    // noncopyable
    ResultsWriter(const ResultsWriter&) = delete;
    ResultsWriter& operator=(const ResultsWriter&) = delete;

    bool write( std::string out_dir_path, std::string out_format );

private:
    /**
     * @param category  Category to print. If empty, everything will be printed.
     * @param stage_idx Stage to generate markdown for.
     */
    std::string generate_markdown
        ( std::string category,
          const uint32_t stage_idx,
          const bool cumulate_times,
          const bool cross_links );

    bool write_markdown( const std::string& path, const std::string& markdown );

    // yes, copying the strings
    bool run_pandoc( std::string in_path,
                     std::string out_path,
                     std::string out_logo_path,
                     std::string out_format,
                     std::string extra_args );

    ResultsData data;
    std::string pandoc_path;
};

#endif /* end of include guard: RESULTSWRITER_H_YBISRCVG */
