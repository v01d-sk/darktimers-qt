#include <QTabWidget>
#include <QHBoxLayout>
#include <QTableWidget>
#include <QString>
#include <QStringList>

#include "finalresults.h"
#include "ui_finalresults.h"
#include "internal_data.h"
#include "resultswriter.h"

static void AddRow (QTableWidget *tw, EventDataContainer *data, double firsttime)
{
    tw->setRowCount(tw->rowCount() + 1);
    tw->setItem(tw->rowCount()-1,0,new QTableWidgetItem(QString::number(data->racer.StartNum))); 
    tw->setItem(tw->rowCount()-1,1,new QTableWidgetItem((data->racer.Name))); 
    int hour, min;
    double sec;
    double overal = data->getOveral();
    unpack_timestamp(overal, hour, min, sec);
    QString time;
    if (data->dqf == false)
    {
        time.sprintf("%02d:%05.02f", min, sec);
        tw->setItem(tw->rowCount()-1,2,new QTableWidgetItem(time));
        unpack_timestamp(overal - firsttime, hour, min, sec);
        time.sprintf("%02d:%05.02f", min, sec);
        tw->setItem(tw->rowCount()-1,3,new QTableWidgetItem(time));
    }
    else
    {
        time="###";
        tw->setItem(tw->rowCount()-1,2,new QTableWidgetItem(time));
        tw->setItem(tw->rowCount()-1,3,new QTableWidgetItem(time));

    }

}

static void FillTable(QTableWidget *tw, std::vector<EventDataContainer *> data, int category = -1)
{
    for (int i=0; i < data.size(); i++)
    {
        double first_time;
        if (category < 0 )
        {
            if (i==0)
                first_time = data.at(i)->getOveral();
            AddRow(tw, data.at(i), first_time);
        }
        else if (data.at(i)->racer.Category == category)
        {
            if (tw->rowCount()==0)
                first_time = data.at(i)->getOveral();
            AddRow(tw, data.at(i), first_time);
        }
    }
    //tw->sortItems(2);
}

static QTableWidget *createTable()
{
    QStringList header;
    header.append(QObject::tr("Starting Number"));
    header.append(QObject::tr("Name"));
    header.append(QObject::tr("Result Time"));
    header.append(QObject::tr("Lost"));
    QTableWidget *tw = new QTableWidget();
    tw->setColumnCount(4);
    tw->setHorizontalHeaderLabels(header);
    QHeaderView* headerView = tw->horizontalHeader();
    headerView->setSectionResizeMode(QHeaderView::Stretch);
    return tw;
}

static bool compareContainer(EventDataContainer *a, EventDataContainer *b)
{
    if (a->dqf)
        return false;
    if (b->dqf)
        return true;
    return (a->getOveral() < b->getOveral());
}

FinalResultsWin::FinalResultsWin(EventData *d, SettingsData *s, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FinalResultsWindow),
    data(d),
    settings(s)
{
    this->ui->setupUi(this);
    this->ui_tab_widget = findChild<QTabWidget *>("category_tab");
}

void FinalResultsWin::Execute()
{
    std::vector<QString>::const_iterator const_iter = settings->categories.begin();
    QTableWidget *tw = createTable();
    this->tables.push_back(tw);
    this->ui_tab_widget->addTab(tw, tr("Overal results"));
    std::vector<EventDataContainer *> container = this->data->getResults();
    std::sort(container.begin(), container.end(), compareContainer);
    FillTable(tw, container); 
    for (int i=0; i < settings->categories.size(); i++)
    {
        printf("Tab for category %s\n", settings->categories.at(i).toStdString().c_str());
        tw=createTable();
        this->tables.push_back(tw);
        this->ui_tab_widget->addTab(tw, settings->categories.at(i));
        FillTable(tw, container, i); 
    }
}

FinalResultsWin::~FinalResultsWin()
{
    delete this->ui;
}

void FinalResultsWin::on_button_box_clicked(QAbstractButton *button)
{
    this->close();
}


