#include <QString>
#include <QStringList>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>

#include <algorithm>
#include <iomanip>
#include <stdio.h>

#include "data_analysis.h"
#include "internal_data.h"
#include "finalresults.h"

#define SEPARATOR ","

static bool compareStages(const StageTime &a, const StageTime &b)
{
    return a.timestamp < b.timestamp;
}

StageTime::StageTime(QString tmac, double time):
    mac(tmac),
    timestamp(time)
{
}

DataAnalysis::DataAnalysis(const std::vector<QString> &files, const std::vector<RacerData> &rData, SettingsData *data):
    settings(data),
    filenames(files),
    racers(rData),
    eventData(NULL)

{
}

void DataAnalysis::Execute()
{
    std::vector<QString>::const_iterator iter = this->filenames.begin();
    for (; iter != this->filenames.end(); ++iter)
    {
        this->LoadFile(*iter);
    }
    FinalResultsWin fin(this->eventData, this->settings);
    fin.Execute();
    fin.exec();    
}
void DataAnalysis::LoadFile(const QString &file)
{
    QFile caFile(settings->interface_result_path + "/" + file);
    std::vector<QString> rfid_vec;
    std::vector<double> sec_vec;

    caFile.open(QIODevice::ReadOnly | QIODevice::Text);
    if(!caFile.isOpen()){
        int ret = QMessageBox::warning(NULL, QObject::tr("Error"), QObject::tr("Can not open file: ") + file, QMessageBox::Ok);
        return;
    }    
    QTextStream inStream(&caFile);
    while (!inStream.atEnd())
    {
        double sec;
        QString rfid;
        if (this->Process(inStream.readLine(), rfid, sec)==false)
        {
            QMessageBox::warning(NULL, QObject::tr("Error"), QObject::tr("Wrong file ") + file, QMessageBox::Ok);
            caFile.close();
            return;
        }
        rfid_vec.push_back(rfid);
        sec_vec.push_back(sec);
    }   
    caFile.close();
    AddResults(file, rfid_vec, sec_vec);
    ResultContainer::iterator iter = this->results.begin();
    for(;iter != this->results.end(); ++iter)
    {
        //if racer had data from all stages (stage_count * 2, because of start and end) 
        if (iter->second.size() == settings->race_stage_names.size() * 2)
        {
            std::vector<StageTime>::iterator stages_iter = iter->second.begin();
            std::sort(iter->second.begin(), iter->second.end(), compareStages);
            stages_iter = iter->second.begin();
            for (;stages_iter != iter->second.end(); ++stages_iter)
            {
                this->stages.push_back(stages_iter->mac);
            }
            break;
        }
    }
    GenerateTimes();
}

void DataAnalysis::GenerateTimes()
{
    this->eventData = new EventData();
    ResultContainer::const_iterator iter = this->results.begin();
    for(;iter != this->results.end(); ++iter)
    {
        std::vector<double> stageResults;
        bool dqf = false;
        for(int j = 0; j < this->stages.size(); j+=2)
        {
            std::vector<StageTime>::const_iterator st1iter;
            st1iter = std::find_if(iter->second.begin(), 
                iter->second.end(),
                [&](const StageTime& f){ return f.mac == stages.at(j); });
            if (st1iter == iter->second.end())
            {
                dqf=true;
                stageResults.push_back(0);
                continue;
            }
            std::vector<StageTime>::const_iterator st2iter;
            st2iter = std::find_if(iter->second.begin(), 
                iter->second.end(),
                [&](const StageTime& f){ return f.mac == stages.at(j+1); });
            if (st2iter == iter->second.end())
            {
                dqf=true;
                stageResults.push_back(0);
                continue;
            }
            stageResults.push_back(st2iter->timestamp - st1iter->timestamp);
        }
        std::vector<RacerData>::const_iterator racer_iter = std::find_if(this->racers.begin(),
                                                              this->racers.end(),
                                                              [&](const RacerData& f){return f == iter->first;});
        if (racer_iter == this->racers.end())
        {
            printf("Holly shit %s\n", iter->first.toStdString().c_str());
            continue;
        }
         
        this->eventData->AddRacerResults(*racer_iter, stageResults, dqf);

    }
}

void DataAnalysis::AddResults(const QString &mac, const std::vector<QString> &rfids, std::vector<double> timestamps)
{
    for (int i=0; i < rfids.size(); i++)
    {
        ResultContainer::iterator iter = this->results.find(rfids.at(i));
        if (iter != this->results.end())
        {
            iter->second.push_back(StageTime(mac, timestamps.at(i)));
        }
        else
        {
            std::vector<StageTime> times;
            times.push_back(StageTime(mac, timestamps.at(i)));
            this->results.insert(std::make_pair(rfids.at(i),times));
        }
    }
}


bool DataAnalysis::Process(const QString &line, QString &rfid, double &seconds)
{
    QStringList list = line.split(SEPARATOR);
    if (list.size() != 2)
        return false;
    rfid = list.at(0);
    QString strsec = list.at(1);
    seconds = strsec.toDouble();
    if (seconds == 0.0)
    {
        QMessageBox::warning(NULL, QObject::tr("Error"), QObject::tr("Can not convert to double"), QMessageBox::Ok);
        return false;
    }
    return true;
}

