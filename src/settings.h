#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

class SettingsData;
class QLineEdit;
class QFormLayout;
class QDateEdit;
class QSpinBox;
class QDateEdit;
class QTextEdit;
class QButonBox;
class QDialogButtonBox;
class QAbstractButton;
class QListWidget;

namespace Ui {
    class SettingsWindow;
}

class SettingsWin : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsWin(SettingsData *data, QWidget *parent = NULL);
    ~SettingsWin();

private slots:
    void on_race_stage_count_valueChanged(int i);
    void on_button_box_clicked(QAbstractButton *button);
    void on_load_logo_clicked();
    void on_result_path_open_clicked();
    void on_pandoc_path_open_clicked();
    void on_add_category_clicked();
    void on_edit_category_clicked();
    void on_remove_category_clicked();

private:
    Ui::SettingsWindow *ui;
    int stage_count;
    std::vector<QLineEdit *> stage_names;
    SettingsData *settings_data;
    QFormLayout *ui_race_tab_layout;
    QLineEdit *ui_server_ip;
    QLineEdit *ui_server_login;
    QLineEdit *ui_server_password;

    QLineEdit *ui_race_name;
    QLineEdit *ui_race_location;
    QLineEdit *ui_race_logo_path;
    QSpinBox *ui_race_stage_count;
    QDateEdit *ui_race_date;
    QTextEdit *ui_race_organizer;

    QLineEdit *ui_result_path;
    QLineEdit *ui_pandoc_path;
    QDialogButtonBox *ui_button_box;
    QListWidget *ui_categories_list;

    bool initialized;
};
#endif
