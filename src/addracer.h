#ifndef ADDRACER_H
#define ADDRACER_H

#include <QDialog>
#include <QString>

namespace Ui {
    class AddRacerWindow;
}

class QLineEdit;
class QComboBox;
class QListWidget;
class SettingsData;
class QAbstractButton;
class RacerData;

class AddRacerWin : public QDialog
{
    Q_OBJECT

public:
    explicit AddRacerWin(SettingsData *settings_data, RacerData *data = NULL, QWidget *parent = 0);
    ~AddRacerWin();
    RacerData *getRacerData();

private slots:
    void on_add_rfid_clicked();
    void on_del_rfid_clicked();
    void on_button_box_clicked(QAbstractButton *button);
private:
    SettingsData *settings;
    QListWidget *ui_rfid_list;
    QLineEdit *ui_racer_name;
    QLineEdit *ui_start_number;
    QLineEdit *ui_team;
    QLineEdit *ui_birth_year;
    QComboBox *ui_category;
    QLineEdit *ui_country;
    RacerData *racer;

    Ui::AddRacerWindow *ui;
};
#endif

