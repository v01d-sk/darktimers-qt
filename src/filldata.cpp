#include <QTableWidget>
#include <QStringList>
#include <QHeaderView>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QModelIndex>
#include <QTextCodec>

#include "filldata.h"
#include "ui_filldata.h"
#include "addracer.h"
#include "internal_data.h"

class Item: public QTableWidgetItem
{
public:
    Item(QString text):
        QTableWidgetItem(text)
    {
    }
    bool operator< (const QTableWidgetItem &other) const
    {
        return (this->text().toInt() < other.text().toInt());
    }
};

FillDataWin::FillDataWin(SettingsData *settings_data, QWidget *parent):
    QDialog(parent),
    ui(new Ui::FillDataWindow),
    settings(settings_data)
{
    ui->setupUi(this);
    this->ui_racers_table = findChild<QTableWidget*>("racers_table");
    QStringList header;
    header.append("Starting Number");
    header.append("Name");
    header.append("Team");
    header.append("Birth Year");
    header.append("Category");
    header.append("Country");
    header.append("RFID");
    this->ui_racers_table->setHorizontalHeaderLabels(header);
    QHeaderView* headerView = this->ui_racers_table->horizontalHeader();
    headerView->setSectionResizeMode(QHeaderView::Stretch);
    this->Load();
}


FillDataWin::~FillDataWin()
{
    delete ui;
}

void FillDataWin::Load()
{
    QFile caFile(this->settings->interface_result_path + "/racers_list.csv");
    caFile.open(QIODevice::ReadOnly | QIODevice::Text);
    if(!caFile.isOpen()){
        int ret = QMessageBox::warning(this, tr("Error"), tr("Can not save file"), QMessageBox::Ok);
        return;
    }    
    QTextStream inStream(&caFile);
    inStream.setCodec(QTextCodec::codecForName("UTF-8"));
    while (!inStream.atEnd())
    {
        RacerData data(inStream.readLine()); //read one line at a time
        addRow(data);
    }    
    caFile.close();
}

void FillDataWin::addRow(const RacerData &data)
{
    this->ui_racers_table->setRowCount(this->ui_racers_table->rowCount()+1);
    this->ui_racers_table->setItem(this->ui_racers_table->rowCount()-1,0,new Item(QString::number(data.StartNum))); 
    this->ui_racers_table->setItem(this->ui_racers_table->rowCount()-1,1,new Item(data.Name)); 
    this->ui_racers_table->setItem(this->ui_racers_table->rowCount()-1,2,new Item(data.Team));
    this->ui_racers_table->setItem(this->ui_racers_table->rowCount()-1,3,new Item(QString::number(data.BirthYear)));
    this->ui_racers_table->setItem(this->ui_racers_table->rowCount()-1,4,new Item(this->settings->categories.at(data.Category)));
    this->ui_racers_table->setItem(this->ui_racers_table->rowCount()-1,5,new Item(data.Country));
    this->ui_racers_table->setItem(this->ui_racers_table->rowCount()-1,6,new Item(data.strRfid()));
    this->ui_racers_table->sortByColumn(0,Qt::AscendingOrder);
}

void FillDataWin::racerFromRow(int row, RacerData &rData)
{
    if (row < this->ui_racers_table->rowCount())
    {
        rData.StartNum = this->ui_racers_table->item(row, 0)->text().toUInt();
        rData.Name = this->ui_racers_table->item(row, 1)->text();
        rData.Team = this->ui_racers_table->item(row, 2)->text();
        rData.BirthYear = this->ui_racers_table->item(row, 3)->text().toUInt();
        rData.Category = this->settings->CategoryId(this->ui_racers_table->item(row, 4)->text());
        rData.Country = this->ui_racers_table->item(row, 5)->text();
        rData.SetRfid (this->ui_racers_table->item(row, 6)->text());
    }
}

void FillDataWin::updateRow(int row, const RacerData &data)
{
    if (row < this->ui_racers_table->rowCount())
    {
        this->ui_racers_table->item(row, 0)->setText(QString::number(data.StartNum));
        this->ui_racers_table->item(row, 1)->setText(data.Name);
        this->ui_racers_table->item(row, 2)->setText(data.Team);
        this->ui_racers_table->item(row, 3)->setText(QString::number(data.BirthYear));
        this->ui_racers_table->item(row, 4)->setText(this->settings->categories.at(data.Category));
        this->ui_racers_table->item(row, 5)->setText(data.Country);
        this->ui_racers_table->item(row, 6)->setText(data.strRfid());
    }
}

void FillDataWin::on_close_clicked()
{
    close();
}

void FillDataWin::on_add_racer_clicked()
{
    
    AddRacerWin addracer(this->settings);
    addracer.exec();
    RacerData *data = addracer.getRacerData();
    if (data != NULL)
    {
        addRow(*data);
        this->ui_racers_table->sortByColumn(0,Qt::AscendingOrder);
    }
}

void FillDataWin::on_save_btn_clicked()
{
    QFile caFile(settings->interface_result_path + "/racers_list.csv");
    caFile.open(QIODevice::WriteOnly | QIODevice::Text);
    if(!caFile.isOpen()){
        int ret = QMessageBox::warning(this, tr("Error"), tr("Can not save file"), QMessageBox::Ok);
        return;
    }    
    QTextStream outStream(&caFile);
    outStream.setCodec(QTextCodec::codecForName("UTF-8"));
    for(int i=0; i < this->ui_racers_table->rowCount(); i++)
    {
        RacerData rData;
        this->racerFromRow(i, rData);
        outStream << rData.toString() << "\n";
    }
    caFile.close();
}

std::vector<RacerData> FillDataWin::getRacers()
{
    std::vector<RacerData> retvec;
    for(int i=0; i < this->ui_racers_table->rowCount(); i++)
    {
        RacerData rData;
        this->racerFromRow(i, rData);
        printf("Racer %s\n",rData.Name.toStdString().c_str());
        retvec.push_back(rData);
    }
    return retvec;
}

void FillDataWin::on_racers_table_clicked(const QModelIndex &index)
{
   this->ui_racers_table->selectRow(index.row());
}

void FillDataWin::on_delete_racer_clicked()
{
    this->ui_racers_table->removeRow(this->ui_racers_table->currentRow());
}

void FillDataWin::on_edit_racer_clicked()
{
    RacerData rData;
    this->racerFromRow(this->ui_racers_table->currentRow(), rData);
    AddRacerWin racerWin(this->settings, &rData);
    racerWin.exec();
    RacerData *getrData = racerWin.getRacerData();
    if (getrData != NULL)
        this->updateRow(this->ui_racers_table->currentRow(), *getrData);
}
