#include <QApplication>
#include <QSettings>

#include "internal_data.h"

#define RACE_NAME "RaceName"
#define RACE_ORGANIZER "RaceOrganizer"
#define RACE_DAY "RaceDay"
#define RACE_MONTH "RaceMonth"
#define RACE_YEAR "RaceYear"
#define RACE_STAGE_NAMES "RaceStageNames"
#define RACE_STAGE "RaceStage"
#define RACE_LOGO "RaceLogo"
#define RACE_LOCATION "RaceLocation"

#define CONNECTION_SERVER "Server"
#define CONNECTION_LOGIN "Login"
#define CONNECTION_PASSWORD "Password"

#define INTERFACE_RESULT_PATH "ResultPath"
#define INTERFACE_PANDOC_PATH "PandocPath"

#define CATEGORIES "Categories"
#define CATEGORY_NAME "CategoryName"

#define DEFAULT_RACE_DAY 1
#define DEFAULT_RACE_MONTH 1
#define DEFAULT_RACE_YEAR 201


#define SEPARATOR "|"


/*
 * Settings Data
 */

SettingsData::SettingsData():
    race_day(DEFAULT_RACE_DAY),
    race_month(DEFAULT_RACE_MONTH),
    race_year(DEFAULT_RACE_YEAR)
{
}

SettingsData::~SettingsData()
{
}

int SettingsData::Save(QString filename)
{
    QString dst;
    if (filename.isEmpty())
        dst = QApplication::applicationDirPath() + "/darktimers.ini";
    else
        dst=filename;
    QSettings settings(dst, QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    settings.setValue(RACE_NAME, this->race_name);
    settings.setValue(RACE_ORGANIZER, this->race_organizer);
    settings.setValue(RACE_DAY, this->race_day);
    settings.setValue(RACE_MONTH, this->race_month);
    settings.setValue(RACE_YEAR, this->race_year);
    settings.setValue(RACE_LOCATION, this->race_location);

    settings.setValue(RACE_LOGO, this->race_logo);
    settings.beginWriteArray(RACE_STAGE_NAMES);
    for (int i = 0; i < this->race_stage_names.size(); ++i) {
        settings.setArrayIndex(i);
        settings.setValue(RACE_STAGE, race_stage_names.at(i));
    }    
    settings.endArray(); 
    settings.beginWriteArray(CATEGORIES);
    for (int i = 0; i < this->categories.size(); ++i) {
        settings.setArrayIndex(i);
        settings.setValue(CATEGORY_NAME, categories.at(i));
    }    
    settings.endArray(); 
    settings.setValue(CONNECTION_SERVER, this->connection_server);
    settings.setValue(CONNECTION_LOGIN, this->connection_login);
    settings.setValue(CONNECTION_PASSWORD, this->connection_password);
    settings.setValue(INTERFACE_RESULT_PATH, this->interface_result_path);
    settings.setValue(INTERFACE_PANDOC_PATH, this->interface_pandoc_path);
    return 0;
}

int SettingsData::Load(QString filename)
{
    QString dst;
    if (filename.isEmpty())
        dst = QApplication::applicationDirPath() + "/darktimers.ini";
    else
        dst=filename;
    QSettings settings(dst, QSettings::IniFormat);
    this->race_name = settings.value(RACE_NAME, "").toString();
    this->race_organizer = settings.value(RACE_ORGANIZER, "").toString();
    this->race_day = settings.value(RACE_DAY, QString::number(DEFAULT_RACE_DAY)).toUInt();
    this->race_month = settings.value(RACE_MONTH, QString::number(DEFAULT_RACE_MONTH)).toUInt();
    this->race_year = settings.value(RACE_YEAR, QString::number(DEFAULT_RACE_YEAR)).toUInt();
    this->race_logo = settings.value(RACE_LOGO, "").toString();
    this->race_location = settings.value(RACE_LOCATION, "").toString();
    int size = settings.beginReadArray(RACE_STAGE_NAMES);
    this->race_stage_names.clear();
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        this->race_stage_names.push_back(settings.value(RACE_STAGE, "").toString());
    }    
    settings.endArray(); 
    size = settings.beginReadArray(CATEGORIES);
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        this->categories.push_back(settings.value(CATEGORY_NAME, "").toString());
    }
    settings.endArray(); 
    this->connection_server = settings.value(CONNECTION_SERVER, "").toString();
    this->connection_login = settings.value(CONNECTION_LOGIN, "").toString();
    this->connection_password = settings.value(CONNECTION_PASSWORD, "").toString();
    this->interface_result_path = settings.value(INTERFACE_RESULT_PATH, "").toString();
    this->interface_pandoc_path = settings.value(INTERFACE_PANDOC_PATH, "").toString();
    return 0;
}

int SettingsData::CategoryId(QString name)
{
    for (int i = 0; i < this->categories.size(); i++)
    {
        if (this->categories.at(i) == name)
            return i;
    }
    return -1;
}

/*
 * Rader Data
 */

RacerData::RacerData():
    StartNum(0),
    BirthYear(0),
    Category(0)
{
}

RacerData::RacerData(QString line)
{
    QStringList items = line.split(SEPARATOR);
    this->SetRfid(items.at(0));
    this->StartNum = items.at(1).toUInt();
    this->Name = items.at(2);
    this->Team = items.at(3);
    this->BirthYear = items.at(4).toUInt();
    this->Category = items.at(5).toUInt();
    this->Country = items.at(6);
}

const QString RacerData::toString() const
{

    return (this->strRfid() + SEPARATOR +
            QString::number(this->StartNum) + SEPARATOR +
            this->Name + SEPARATOR +
            this->Team + SEPARATOR +
            QString::number(this->BirthYear) + SEPARATOR +
            QString::number(this->Category) + SEPARATOR +
            this->Country);
}

const QString RacerData::strRfid() const
{
    QString result;
    if (this->Rfid.size() > 0)
    {
        for (int i = 0; i < this->Rfid.size() -1; i++)
        {
            result += this->Rfid.at(i) + ",";
        }
        result += this->Rfid.at(this->Rfid.size()-1);
    }
    return result;
}

void RacerData::SetRfid(const QString &strRfid)
{
    this->Rfid.clear();
    QStringList rfidlist = strRfid.split(",");
    QStringList::iterator iter = rfidlist.begin();
    for ( ;iter != rfidlist.end(); ++iter)
    {
        this->Rfid.push_back(*iter);
    }
}

void RacerData::SetRfid(const std::vector<QString> &Rfids)
{
    this->Rfid.clear();
    this->Rfid.insert(this->Rfid.end(), Rfids.begin(), Rfids.end());
}

bool RacerData::operator == (const QString &strRfid) const
{
    return(std::find(this->Rfid.begin(), this->Rfid.end(), strRfid) == this->Rfid.end() ? false : true);
}

bool RacerData::operator != (const QString &strRfid) const
{
    return(std::find(this->Rfid.begin(), this->Rfid.end(), strRfid) == this->Rfid.end() ? true : false);
}

bool RacerData::operator == (unsigned int t) const
{
    return(this->StartNum == t);
}

bool RacerData::operator != (unsigned int t) const
{
    return(this->StartNum != t);
}


/*
 * EventData
 */
EventDataContainer::EventDataContainer(const RacerData &r, const std::vector<double> &t, bool d):
    racer(r),
    stagesTimes(t),
    dqf(d)
{
}
double EventDataContainer::getOveral()
{
    double overal=0;
    std::vector<double>::iterator iter = this->stagesTimes.begin();
    for (; iter != this->stagesTimes.end(); ++iter)
    {
        overal += *iter;
    }
    return overal;
}

EventData::EventData()
{
}

void EventData::AddRacerResults(const RacerData &data, const std::vector<double> &times, bool dqf)
{
   this->results.push_back(new EventDataContainer(data, times, dqf));
}

unsigned int EventData::size()
{
    return this->results.size();
}

std::vector<EventDataContainer *> EventData::getResults()
{

    return this->results;
}
