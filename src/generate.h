#ifndef GENERATE_H
#define GENERATE_H
#include "resultswriter.h"

class EventData;
class EventDataContainer;
class SettingsData;

class GenerateResults
{
    public:
        GenerateResults(EventData *e, SettingsData *s);
        void Execute();
    private:
        Racer CreateRacer(EventDataContainer *data);

        EventData *eventData;
        SettingsData *settings;
};
#endif
