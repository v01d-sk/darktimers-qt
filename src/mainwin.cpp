#include <QApplication>
#include "mainwin.h"
#include "ui_mainwin.h"

#include "settings.h"
#include "filldata.h"
#include "internal_data.h"
#include "data_reader.h"
#include "data_analysis.h"
#include "generate.h"

MainWin::MainWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    eventData(NULL)
{
    ui->setupUi(this);
    this->settings_data = new SettingsData();
    this->settings_data->Load("");
    this->filldatawin = new FillDataWin(this->settings_data);
    this->racers = this->filldatawin->getRacers();
}

MainWin::~MainWin()
{
    delete ui;
    delete this->settings_data;
}

//slots

void MainWin::on_fill_data_clicked()
{
    this->filldatawin->exec();
    this->racers = this->filldatawin->getRacers();
}

void MainWin::on_read_data_clicked()
{
    DataReader reader(this->settings_data);
    reader.Execute();
    this->filenames = reader.getReceivedFiles();
}

void MainWin::on_settings_clicked()
{
    SettingsWin settings(this->settings_data, this);
    settings.exec();
}

void MainWin::on_process_data_clicked()
{
    DataAnalysis analysis(this->filenames, this->racers, this->settings_data);
    analysis.Execute();
    this->eventData = analysis.getEventData();
}

void MainWin::on_generate_results_clicked()
{
    GenerateResults gen(this->eventData, this->settings_data);
    gen.Execute();
}

void MainWin::on_exit_clicked()
{
    QApplication::quit();
}

