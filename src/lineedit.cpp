#include <QLineEdit>
#include <QLabel>
#include <QAbstractButton>

#include "lineedit.h"
#include "ui_lineedit.h"

LineEditWin::LineEditWin(const QString &title, const QString &text, QWidget *parent):
    QDialog(parent),
    ui(new Ui::LineEditWindow)
{
    ui->setupUi(this);
    this->ui_value = findChild<QLineEdit*>("value");
    QLabel *label = findChild<QLabel*>("label");
    setWindowTitle(title);
    label->setText(text);
}

LineEditWin::~LineEditWin()
{
    delete this->ui;
}

const QString LineEditWin::value() const
{
    return this->ui_value->text();
}

void LineEditWin::on_button_box_clicked(QAbstractButton *button)
{
    if (button->text()!="OK")
    {
        this->ui_value->setText("");
    }
    this->close();
}

