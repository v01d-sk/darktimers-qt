#ifndef FILLDATA_H
#define FILLDATA_H

#include <QDialog>
#include <vector>
#include "internal_data.h"

namespace Ui {
    class FillDataWindow;
}

class QTableWidget;
class QModelIndex; 

class SettingsData;
class RacerData;

class FillDataWin : public QDialog
{
    Q_OBJECT

public:
    explicit FillDataWin(SettingsData *settings_data, QWidget *parent = 0);
    void Load();
    ~FillDataWin();
    std::vector<RacerData> getRacers();

private slots:
    void on_close_clicked();
    void on_add_racer_clicked();
    void on_save_btn_clicked();
    void on_delete_racer_clicked();
    void on_racers_table_clicked(const QModelIndex &index);
    void on_edit_racer_clicked();
private:
    void addRow(const RacerData &data);
    void racerFromRow(int rowNum, RacerData &data);
    void updateRow(int row, const RacerData &data);
    QTableWidget *ui_racers_table;
    SettingsData *settings;
    Ui::FillDataWindow *ui;
};
#endif

