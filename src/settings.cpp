#include <QString>
#include <QFormLayout>
#include <QLineEdit>
#include <QWidget>
#include <QSpinBox>
#include <QDateEdit>
#include <QTextEdit>
#include <QDate>
#include <QDialogButtonBox>
#include <QAbstractButton>
#include <QFileDialog>
#include <QListView>

#include <stdio.h>

#include "settings.h"
#include "ui_settings.h"
#include "internal_data.h"
#include "category.h"


SettingsWin::SettingsWin(SettingsData *data, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsWindow),
    stage_count(0),
    settings_data(data),
    initialized(false)
{
    this->ui->setupUi(this);
    this->ui_race_tab_layout = findChild<QFormLayout*>("race_tab_layout");

    this->ui_server_ip = findChild<QLineEdit*>("server_ip");
    this->ui_server_ip->setText(this->settings_data->connection_server);

    this->ui_server_login = findChild<QLineEdit*>("server_login");
    this->ui_server_login->setText(this->settings_data->connection_login);

    this->ui_server_password = findChild<QLineEdit*>("server_password");
    this->ui_server_password->setText(this->settings_data->connection_password);

    this->ui_race_name = findChild<QLineEdit*>("race_name");
    this->ui_race_name->setText(this->settings_data->race_name);

    this->ui_race_location = findChild<QLineEdit*>("race_location");
    this->ui_race_location->setText(this->settings_data->race_location);

    this->ui_race_logo_path = findChild<QLineEdit*>("race_logo_path");
    this->ui_race_logo_path->setText(this->settings_data->race_logo);

    this->ui_race_stage_count = findChild<QSpinBox*>("race_stage_count");
    this->ui_race_stage_count->setValue(this->settings_data->race_stage_names.size());
    if (this->settings_data->race_stage_names.size() > 0)
    {
        for (int i=0; i < this->settings_data->race_stage_names.size(); i++)
        {
            QLineEdit *edit = new QLineEdit();
            edit->setText(this->settings_data->race_stage_names.at(i));
            this->stage_names.push_back(edit);
            QString tmpname = QString("Stage ") + QString::number(this->stage_names.size()) + QString(" Name:");
            this->ui_race_tab_layout->addRow(tmpname, edit);
        }
    }
    this->ui_race_date = findChild<QDateEdit*>("race_date");
    this->ui_race_date->setDate(QDate(this->settings_data->race_year, this->settings_data->race_month, this->settings_data->race_day));

    this->ui_race_organizer = findChild<QTextEdit*>("race_organizer");
    this->ui_race_organizer->setText(this->settings_data->race_organizer); 

    this->ui_result_path = findChild<QLineEdit*>("result_path");
    this->ui_result_path->setText(this->settings_data->interface_result_path);

    this->ui_pandoc_path = findChild<QLineEdit*>("pandoc_path");
    this->ui_pandoc_path->setText(this->settings_data->interface_pandoc_path);

    this->ui_categories_list = findChild<QListWidget*>("categories_list");
    for (int i=0; i < this->settings_data->categories.size(); i++)
    {
        this->ui_categories_list->addItem(this->settings_data->categories.at(i));
    }
}

SettingsWin::~SettingsWin()
{
    delete this->ui;
}

void SettingsWin::on_race_stage_count_valueChanged(int i)
{
    if (!this->initialized)
    {
        this->initialized=true;
        return;
    }
    if (i < 0)
        return;
    if (i > this->stage_count)
    {
        QLineEdit *edit = new QLineEdit();
        this->stage_names.push_back(edit);
        QString tmpname = QString("Stage ") + QString::number(this->stage_names.size()) + QString(" Name:");
        this->ui_race_tab_layout->addRow(tmpname, edit);
    }
    else if (i < this->stage_count)
    {
        int row = ui_race_tab_layout->rowCount()-1;
        /*
         * !!!!!!!!!!!!!!!
         * After update to QT 5.8 remove this code and reimplement it with removeRow/takeRow method
         * THIS CODE IS BIG HACK REQUIRED BY STUPIDITY OF QT DEVELOPERS. IN QT LOWER THAN 5.8 THERE
         * IS NO REVERSE FUNCTION TO ADDROW
         * !!!!!!!!!!!!!!!
         */
        QLayoutItem* editItem = this->ui_race_tab_layout->itemAt(row, QFormLayout::FieldRole);
        QLayoutItem* labelItem = this->ui_race_tab_layout->itemAt(row, QFormLayout::LabelRole);
        while (row != 0)
        {
            if (editItem == NULL || labelItem==NULL)
            {
                row--;
                editItem = this->ui_race_tab_layout->itemAt(row, QFormLayout::FieldRole);
                labelItem = this->ui_race_tab_layout->itemAt(row, QFormLayout::LabelRole);
            }
            else
            {
                this->ui_race_tab_layout->removeItem(editItem);
                this->ui_race_tab_layout->removeItem(labelItem);
                delete(editItem->widget());
                delete(labelItem->widget());
                delete(editItem);
                delete(labelItem);
                this->ui_race_tab_layout->invalidate();
                this->stage_names.pop_back();
                break;
            }
        }
    }
    this->stage_count = i;
    
}

void SettingsWin::on_button_box_clicked(QAbstractButton *button)
{
    if (button->text() == "OK")
    {
        this->settings_data->race_name = this->ui_race_name->text();
        this->settings_data->race_location = this->ui_race_location->text();
        this->settings_data->race_logo = this->ui_race_logo_path->text();
        this->settings_data->race_organizer = this->ui_race_organizer->toPlainText();
        this->settings_data->race_stage_names.clear();
        QDate date = this->ui_race_date->date();
        this->settings_data->race_day = date.day();
        this->settings_data->race_month = date.month();
        this->settings_data->race_year = date.year();
        this->settings_data->race_stage_names.clear();
        for (int i=0; i < this->stage_names.size(); i++)
        {
            this->settings_data->race_stage_names.push_back(this->stage_names.at(i)->text());
        }
        this->settings_data->connection_server = this->ui_server_ip->text();
        this->settings_data->connection_login = this->ui_server_login->text();
        this->settings_data->connection_password = this->ui_server_password->text();
        this->settings_data->interface_result_path = this->ui_result_path->text();
        this->settings_data->interface_pandoc_path = this->ui_pandoc_path->text();
        this->settings_data->categories.clear();
        for (int i=0; i < this->ui_categories_list->count(); i++)
        {
            this->settings_data->categories.push_back(this->ui_categories_list->item(i)->text());
        }

        this->settings_data->Save("");
        
    }
    close();
}

void SettingsWin::on_load_logo_clicked()
{
    this->ui_race_logo_path->setText(QFileDialog::getOpenFileName(this,
        tr("Select Image"), "",
        tr("Images (*.png *.jpg *.gif *.svg);;All Files (*)")));

}
void SettingsWin::on_result_path_open_clicked()
{
    this->ui_result_path->setText(QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "/home",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks));
}
void SettingsWin::on_pandoc_path_open_clicked()
{
    this->ui_pandoc_path->setText(QFileDialog::getOpenFileName(this,
        tr("Select Pandoc Command"), "",
        tr("All Files (*)")));
}

void SettingsWin::on_add_category_clicked()
{
    CategoryWin win("Add Category","");
    win.exec();
    QString data = win.getCategoryName();
    if (!data.isEmpty())
        this->ui_categories_list->addItem(data);
}

void SettingsWin::on_edit_category_clicked()
{
    CategoryWin win("Edit Category",this->ui_categories_list->currentItem()->text());
    win.exec();
    QString data = win.getCategoryName();
    if (!data.isEmpty())
        this->ui_categories_list->currentItem()->setText(data);     
}

void SettingsWin::on_remove_category_clicked()
{
    this->ui_categories_list->takeItem(this->ui_categories_list->currentRow());
}

