#ifndef LINEEDIT_H
#define LINEEDIT_H

#include <QDialog>
#include <QString>

namespace Ui {
    class LineEditWindow;
}

class QLineEdit;
class QAbstractButton;

class LineEditWin : public QDialog
{
    Q_OBJECT

public:
    explicit LineEditWin(const QString &title, const QString &label, QWidget *parent = 0);
    ~LineEditWin();
    const QString value() const;
private slots:
    void on_button_box_clicked(QAbstractButton *button);
private:
    QLineEdit *ui_value;
    Ui::LineEditWindow *ui;
};
#endif

