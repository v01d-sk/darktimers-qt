#ifndef DATA_READER_H
#define DATA_READER_H
#include <QObject>
#include <vector>
#include <QString>

class SettingsData;
class DownloaderThread;
class ProgressBarWin;

/*! 
 * Read data from sftp server stored in SettingsData
 */
class DataReader
{
    public:
        DataReader(SettingsData *data);
        ~DataReader();
        void Execute();
        std::vector<QString> getReceivedFiles();
    private:
        SettingsData *settings;
        DownloaderThread *thread;
        ProgressBarWin *progresswin;
        std::vector<QString> filenames;
};

#endif
