#ifndef CATEGORY_H
#define CATEGORY_H

#include <QDialog>
#include <QString>


namespace Ui {
    class CategoryWindow;
}

class QLineEdit;
class QAbstractButton;

class CategoryWin : public QDialog
{
    Q_OBJECT

public:
    explicit CategoryWin(QString title, QString name, QWidget *parent = 0);
    ~CategoryWin();
    QString getCategoryName();
private slots:
    void on_button_box_clicked(QAbstractButton *button);
private:
    QLineEdit *ui_category_name;    
    Ui::CategoryWindow *ui;
};
#endif

