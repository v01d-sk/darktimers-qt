#ifndef DATA_ANALYSIS_H
#define DATA_ANALYSIS_H

#include <vector>
#include <map>
#include <QString>
#include  "internal_data.h"
class SettingsData;

/*!
 * Analyze data after transfer from sftp Server
 */
//                MAC,    TIMESTAMP
struct StageTime
{
    StageTime(QString tmac, double time); 
    QString mac;
    double timestamp;
};
//                RFID
typedef std::map <QString, std::vector<StageTime>> ResultContainer;

class DataAnalysis
{
    public:
        DataAnalysis(const std::vector<QString> &filenames, const std::vector<RacerData> &rData, SettingsData *data);
        void Execute();
        EventData *getEventData(){return this->eventData;};
    private:
        void LoadFile(const QString &file);
        bool Process(const QString &line, QString &rfid, double &seconds);
        void AddResults(const QString &mac, const std::vector<QString> &rfid, std::vector<double> seconds);
        void GenerateTimes();

        std::vector<QString> files;
        ResultContainer results;
        SettingsData *settings;
        std::vector<QString> filenames;
        //Sorted stages
        std::vector<QString> stages;
        EventData *eventData; 
        const std::vector<RacerData> &racers;
        
};

#endif //DATA_ANALYSIS_H
