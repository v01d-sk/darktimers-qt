#include <QString>
#include <QThread>
#include <QByteArray>

#include <time.h>

#ifdef WIN32
#include <winsock2.h>

#define __S_IFDIR       0040000 /* Directory.  */
#define __S_IFMT        0170000 /* These bits determine file type.  */
#define __S_ISTYPE(mode, mask)  (((mode) & __S_IFMT) == (mask))
#define S_ISDIR(mode)    __S_ISTYPE((mode), __S_IFDIR)

#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#endif
extern "C" {
#include <libsftp.h>
}
#include "data_reader.h"
#include "progressbar.h"
#include "internal_data.h"

#define SRCDIR "/results/"
class DownloaderThread : public QThread
{
public:
    explicit DownloaderThread(SettingsData *data, ProgressBarWin *progress):
        settings(data),
        win(progress)
    {
    }

    void run()
    {
        libsftp_session *session;
        long hostaddr;
        int rc;
        this->win->set(0);
        this->filenames.clear();
        QByteArray ba_ip = this->settings->connection_server.toLatin1();
        const char *ipaddr = ba_ip.data();
        QByteArray ba_login = this->settings->connection_login.toLatin1();
        const char *login = ba_login.data();
        QByteArray ba_password = this->settings->connection_password.toLatin1();
        const char *password = ba_password.data();

        hostaddr = inet_addr(ipaddr);
        session = libsftp_connect_password(hostaddr, login, password);
        std::vector<char *> filenames;
        if (session != NULL)
        {
            // TODO written without use of brain, rewrite this ASAP.
            char src_dir_full[1024];
            strncpy(src_dir_full, SRCDIR, sizeof(src_dir_full) - 1);
            time_t now = time(NULL);
            struct tm *t = localtime(&now);
            strftime(src_dir_full + strlen(SRCDIR), sizeof(src_dir_full) - strlen(SRCDIR) - 1, "%Y-%m-%d", t);
            printf("%s\n", src_dir_full);

            libsftp_filelist *tmpdata = libsftp_opendir(session, src_dir_full);
            if (tmpdata != NULL)
            {
                libsftp_filelist *current, *last;
                current = tmpdata;
                while (current != NULL)
                {
                    if (!S_ISDIR(current->permissions) && strcmp(strrchr(current->filename,'.'),".csv") == 0)
                    {
                        filenames.push_back(current->filename);
                        last = current;
                        current = current->next;
                        free(last);
                    }
                    else
                    {
                        last=current;
                        current = current->next;
                        free(last->filename);
                        free(last);
                    }
                }
            }
            this->win->set(5);
            int progresscounter = 95 / filenames.size() + 1;
            int currcount = 5;
            for (int i = 0; i < filenames.size(); i++)
            {
                QString qsrc = QString(src_dir_full) + "/" + filenames.at(i);
                QByteArray ba_src = qsrc.toLatin1();
                QString qdst = this->settings->interface_result_path + "/" + filenames.at(i);
                this->filenames.push_back(QString(filenames.at(i)));
                QByteArray ba_dst = qdst.toLatin1();
                libsftp_get(session, ba_src.data(), ba_dst.data());
                free(filenames.at(i)); 
                currcount += progresscounter;
                if (currcount > 100)
                    currcount = 100;
                this->win->set(currcount);
            }
            libsftp_disconnect(session);
        }
        this->win->close();
    }

    std::vector<QString> getReceivedFiles()
    {
        return this->filenames;
    }
private:
    SettingsData *settings;
    ProgressBarWin *win;
    std::vector<QString> filenames;
};


DataReader::DataReader(SettingsData *data):
    settings(data)
{
    this->progresswin = new ProgressBarWin("Download", "Downloading result files", true);
    this->thread = new DownloaderThread(this->settings, this->progresswin);
}

DataReader::~DataReader()
{
    delete(this->thread);
    delete(this->progresswin);
}

void DataReader::Execute()
{
    this->thread->start();
    this->progresswin->exec();
    this->thread->wait();
}

std::vector<QString> DataReader::getReceivedFiles()
{
    return this->thread->getReceivedFiles();
}
