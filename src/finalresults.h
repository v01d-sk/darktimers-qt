#ifndef FINALRESULTS_H
#define FINALRESULTS_H

#include <QDialog>
#include <QString>
#include <vector>

namespace Ui
{
    class FinalResultsWindow;
};

class QTabWidget;
class QAbstractButton;
class QTableWidget;
class EventData;
class SettingsData;


class FinalResultsWin : public QDialog
{
    Q_OBJECT

public:
    explicit FinalResultsWin(EventData *data, SettingsData *sdata, QWidget *parent = 0);
    ~FinalResultsWin();
    void Execute();
private slots:
    void on_button_box_clicked(QAbstractButton *button);

private:
    QTabWidget *ui_tab_widget;
    EventData *data;
    SettingsData *settings;
    Ui::FinalResultsWindow *ui;
    std::vector<QTableWidget *> tables;
};
#endif

